import os
import json
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('escluster')


def test_es_cluster_status(host):
    cmd = host.run("curl http://localhost:9200/_cluster/health")
    assert cmd.rc == 0
    status = json.loads(cmd.stdout)
    assert status['status'] == 'green'
    assert status['number_of_data_nodes'] == status['number_of_nodes'] == 3
