ics-ans-role-elasticsearch
===================

Ansible role to install elasticsearch.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
elasticsearch_version: "5.6.12"  # Graylog doesn't work with es version 6+
elasticsearch_rpm_url: "https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-{{ elasticsearch_version }}.rpm"
elasticsearch_cluster_name: elasticcluster
elasticsearch_node_name: "{{ ansible_hostname.split('.') | first | lower }}"
elasticsearch_bindips:
  - 192.168.77.11  # or _eth1_ to use the address assigned to eth1
  - 127.0.0.1
elasticsearch_memory_lock: false
```
To configure an elasticsearch cluster, define the ```elasticsearch_cluster_groupname``` variable with the name of ansible group containing the hosts for the cluster.

Note: while elasticsearch allows to specify a binding ip by its interface name (i.e. "_eth1_"), please gather the IP address using ansible.
Example:
```yaml
elasticsearch_bindips:
  - "{{ ansible_eth1['ipv4']['address'] }}"
  - 127.0.0.1
```


Example Playbook
----------------

```yaml
- hosts: elasticsearchers
  roles:
    - role: ics-ans-role-elasticsearch
```

License
-------

BSD 2-clause
